
\newcommand{\CLASSINPUTtoptextmargin}{1cm}
\newcommand{\CLASSINPUTbottomtextmargin}{1cm}
\documentclass[conference, a4paper]{IEEEtran}

\IEEEoverridecommandlockouts

\ifCLASSINFOpdf
\else
\fi

\input{./header.tex}

\setlength{\textfloatsep}{10pt plus 1.0pt minus 2.0pt}
\newcommand{\card}[1]{\ensuremath{\left\lvert #1 \right\rvert}}

\include{pics}

\usepackage{url}

\begin{document}

\newtheorem{algorithm}{Algorithm}
\newtheorem{method}{Sorting method}

\setlength{\abovedisplayshortskip}{0pt}
\setlength{\belowdisplayshortskip}{0pt} 

%\title{Quality over Quantity - True Friendship in Opportunistic Wireless Mesh Networks}
\title{Header Compression in Opportunistic Routing}

%\author{\IEEEauthorblockN{Mate T\"om\"osk\"ozi\IEEEauthorrefmark{1}, Ievgenii Tsokalo\IEEEauthorrefmark{1}, Sreekrishna Pandi\IEEEauthorrefmark{1}, and Frank H.P. Fitzek\IEEEauthorrefmark{1}}
%\IEEEauthorblockA{\IEEEauthorrefmark{1}Deutsche Telekom Chair of Communication Networks\\
%Technische Universit\"at Dresden, 01062 Dresden, Germany\\
%Email: \{firstname\}.\{lastname\}@tu-dresden.de
%}
%}

\author{ \IEEEauthorblockN{M\'{a}t\'{e} T\"{o}m\"{o}sk\"{o}zi$^{1,2}$, Ievgenii Tsokalo$^{1}$, Sreekrishna Pandi$^{1}$, Frank H.P. Fitzek$^{1}$, P\'{e}ter Ekler$^{2}$}
    \IEEEauthorblockA{$^{1}$Deutsche Telekom Chair of Communication Networks, Technische Universit\"{a}t Dresden, Germany}
	\IEEEauthorblockA{$^{2}$Department of Automation and Applied Informatics, Budapest University of Technology and Economics, Hungary}

    \IEEEauthorblockA{Email: mate.tomoskozi@[aut.bme.hu, tu-dresden.de], ievgenii.tsokalo@tu-dresden.de, \\ sreekrishna.pandi@tu-dresden.de, frank.fitzek@tu-dresden.de, ekler.peter@aut.bme.hu  }
    \vspace{-0.8cm} }

% make the title area
\maketitle
\begin{abstract}

Next generation use--cases of wireless mesh networks, especially the Massive Machine Type Communications and IoT applications require the capability to handle a large number of connections while maintaining a low--energy usage.  In such cases, the transmissions between source and destination can employ multiple paths. The throughput in a scenario like this can be maximised with application of Opportunistic Routing (OpR). 

In high density networks, the OpR uses the diversity of the broadcast channel to increase the achievable data rates in comparison to traditional routing solutions. However, the header size of the routing messages increases when the density grows. The reduction of this overhead could potentially improve the overall throughput and decrease the battery usage of network heavy applications via diminished wireless interface activity. 

Normally, header compression is employed to minimise the overhead of IP--based cellular traffic between two connected peers. This paper presents for the first time the application of header compression concepts to an opportunistic routing protocol.

We use a packet erasure channel simulator to employ these header compression techniques on the routing messages based on real world mesh network traces. We thereby decrease the size of the routing messages in these simulated topologies and find that they can reduce the size of transmission heavy header elements by at least 50~\% and can be even as high as 85~\% under certain conditions. This increases the potential of opportunistic routing in very large mesh networks, as the reduction in feedback message sizes would increase the maximum achievable data rates exponentially.

\end{abstract}

\begin{IEEEkeywords}Opportunistic routing, header compression \end{IEEEkeywords}


\IEEEpeerreviewmaketitle


\section{Introduction}

The Internet of Things (IoT) has diverse service requirements including data rate, latency, resilience, and low power consumption. The key difference of IoT to the majority of existing networks is the high density and interconnection of the devices. In most cases, all network nodes of a mesh network can communicate between each other over multiple communication paths. In this setup, a proper routing protocol is of vital importance for all key performance indicators of IoT.

In wireless mesh networks, many routing protocols such as OLSR, AODV are adopted with some modifications from the world of wired networks. However, in high density wireless networks, the routing protocol performance can be significantly improved using \emph{Opportunistic Routing (OpR)}. In contrast to OLSR, AODV, and B.A.T.M.A.N, which are Single--Path Routing protocols (SPRs), OpR uses the property of the broadcast medium to utilise multiple communication paths.

With the employment of OpR, it is possible to increase the data rate on the lossy broadcast communication channel using the spatial channel diversity between each sender and the group of receivers \cite{approachinglimits}. The channel diversity can be also used to provide more robust communication, exploiting several parallel paths \cite{resilienceopr}. In low power networks, OpR allows the delay to be decreased through higher selectiveness of the forwarding candidates \cite{lowpoweropr}.

The routing is called opportunistic when the routing decision is based on a stohastic process. In SPR, each sending node has only one forwarding node, which is selected before the data transmission takes place. Hence, SPR has no stochastic process in design. In OpR, for the data rate and the resilience increase, each sender selects several neighbouring nodes for cooperation (receiving group). On a wireless communication channel, which has a broadcasting and lossy nature, such neighbours receive partially different fragments from the sent message (the level of difference depends on the channel correlation). However, they do not know which part is different. Since a similar part should be forwarded only by one node, they should cooperate. Sending feedback is not effective enough because their number scales exponentially with the number of the cooperating nodes. OpR allows the reduction of the number of feedbacks by instructing the cooperating nodes to forward each packet (part of the received fragment) with a certain probability \cite{ExORBiswasMorris, ccack, soar}. Hence, the routing decision is opportunistic.

However, it is not possible to completely avoid forwarding similar information as long as the forwarding decision is applied for each single packet \cite{approachinglimits}. In several works, the performance of OpR is improved with the application of Network Coding (NC) \cite{more, AdaptiveRelayPlayNCool, SPDynamicWM, knowyourneighbours, approachinglimits}. With NC, the amount of similar information forwarded by different neighbours of the same sender can be significantly reduced. Theoretically, it can be reduced to zero if the coding block size is infinitely large \cite{approachinglimits}. 

The gain of OpR over SPR in data rate is shown to be non--negative for many mesh network topologies \cite{diss}. However, the OpR header size is not considered by \cite{diss}. Normally, the OpR header is greater than the SPR header due to the inclusion of information for each cooperating node.

We aim to reduce the size of the OpR header with the application of header compression. As studied in \cite{lowpoweroprmate}, header compression in general allows the reduction of the amount of data transmitted and, consequently, saves energy, which is of key importance for IoT devices. Normally, header compression is employed to minimise the overhead of IP--based cellular traffic between two connected peers. As far as the authors of this work know, this study presents the application of header compression techniques in OpR for the first time. OpR is implemented below the network layer of an OSI model. Therefore, most header compression schemes, such as Robust Header Compression version~2 \cite{rfc5225} or IP Header Compression \cite{IPHC}, can be deployed on the IP layer independent of the header compression employed in OpR. 

% For interoperability of mulitple devices from different vendors in the IoT, the communication devices should support IP, which is widely accepted in the networking community. Previously, the adoption of IP--based transmissions has resulted in an undesirably large protocol overhead due to the small ratio between the payload and packet header sizes. As IP packets typically contain the protocol encapsulations of different higher--layer protocols, the common packetisation overhead is independent from the actual payload sizes, i.e., the control metadata can be quite large compared to the user level payload. 


% For real--time transmissions, a common protocol combination is IP/UDP/RTP, accounting for at least 40 bytes with IPv4 and 60 bytes with IPv6. Past efforts targeted a reduction of these protocol encapsulation overheads, as potential results quickly multiply due to the universality of the employed protocols.  A decrease in the amount  of data requiring transport additionally yields indirect benefits in shorter delays and network interface activity, both of which can reduce the energy footprints of providers and users alike.

Several header compression mechanisms for different protocol combinations were proposed in the past prior to the initial Robust Header Compression version (\mbox{RoHCv1}), which is specified in RFC 3095~\cite{rfc3095}. The first widely used header compression algorithm was CTCP, which was introduced by Van Jacobson in~\cite{VJHC} and focuses on
the TCP/IP protocol combination.  IP Header Compression
(IPHC)~\cite{IPHC} provides several enhancements to CTCP, especially
support for UDP and IPv6, while the Compressed Real Time Protocol
(CRTP)~\cite{CRTP} added support for RTP.  Several enhancements and
modifications for specific scenarios based on these main protocols
exist.
For a brief overview of these header compression schemes see, e.g., \cite{FiHeSeRe04} or \cite{tdk}. RoHCv1 itself was incorporated into 3GPP--UMTS, WiMAX and LTE (PDCP) networks with significant industry support to date and recently revised as RoHCv2 in RFC 5225~\cite{rfc5225}.  The new version increases the use--cases of RoHCv1 as well as the enhancements proposed in--between in RFCs 3843, 4019, 4224, 4995, or 4997 without rendering any parts obsolete. The compression techniques used by these well--known implementations can be also utilised in OpR, which is the main topic of this paper.

The remainder of this paper is orgainsed as follows. The OpR header of the chosen benchmark implementation is described in Section~\ref{seq:simmodel}. The header compression gain is estimated by means of the simulation, which is initialised with real traces of wireless channel measurements of a mesh network environment. The measurement methodology is described in Section~\ref{sec:mnat}. In Section~\ref{sec:simres}, we describe the performance metrics and present the simulation results. Finally, Section~\ref{sec:conclusions} contains the conclusions of this work.

\section{Model Description}
\label{seq:simmodel}

We implement header compression in the ANChOR OpR protocol introduced in \cite{diss}, which, as of yet, shows the best performance in throughput among available OpR solutions. 

\subsection{The OpR Header}

The typical fields of an ANChOR OpR header contain the routing metric and the configuration data for the cooperating nodes. They are repeatedly inserted in the header of each packet even though the change is not significant. Therefore these are prime candidates for compression. Here, we describe the metrics that are employed by the ANChOR OpR header.

The routing metric is important for ANChOR for the selection of the routes. Basically, a node having a smaller routing metric than indicated in the packet header permanently refuses to forward such packet. Each ANChOR node calculates its routing metric autonomously based on the feedback information of its cooperating nodes. This metric is also referred to as the node \textit{priority}. For a certain node $v$, this can be calculated as follows \cite{approachinglimits}:
\begin{equation}
 \label{eq:prioirtymain}
 p(v)=\frac{a(v)}{1 + \sum\limits_{u\in L(v)\setminus w}b(u,v)/p(u)},
\end{equation}
where $w$ is the destination, $a(v)$ is a measure of the information flowing over the cut between $v$ and its cooperating nodes $L(v)$, and $b(u,v)$
is the measure of the information that will be forwarded by a certain cooperating node $u\in L(v)$ when $v$ sends
one unit of information. Interested readers can find the general formulation of the coefficients $a(v)$ and $b(u,v)$ in \cite{approachinglimits}. Depending on the definition of the channel quality, the expressions for $a(v)$ and $b(u,v)$ obtain a different form. We create the channel model based on the testbed measurement results, which are described in Section~\ref{sec:mnat}. From the measurement we obtain a sequence of bits interpreting the success or failure of the packet delivery. Assuming the channel quality between a pair of nodes $u$ and $v$ to be defined through a bit sequence $\mathbf{r}_{(v,u)}$, the coefficients $a(v)$ and $b(u,v)$ can be defined as follows:
\begin{equation}
\label{eq:aandb}
\begin{aligned}
  \ & a(v)=d(v)\cdot [1-(1 -||\cup_{u\in L(v)}\mathbf{r}_{(v,u)}||^2/l)];\\
  \ & b(u,v)=d(v)\cdot (1-(1 -||\mathbf{r}_{(v,u)}||^2/l))\cdot\\
   \ & \ \ \ \ \ \ \ \ \ \ \ (1 -||\cup_{u'\in X(v,u)}\mathbf{r}_{(v,u')}||^2/l),
\end{aligned}
\end{equation}
where all $\mathbf{r}_{(v,u)}$ sequences have the same length $l$, $d(v)$ is the sending data rate, $X(v,u)$ is such 
subset of $L(v)$ so that $\forall u'\in X(v,u):\ p(u)<p(u')$, and $||\mathbf{r}||^2$ is the norm of the vector $\mathbf{r}$ that equals the number of ones in the binary vector.

Another important ANChOR metric is the \emph{filtering coefficient} (FC). Each sender instructs its cooperating nodes about the part of the received information which should be forwarded. Since ANChOR uses Network Coding, FC translates to the number of coded packets to be sent for each received one. In ANChOR, the sender attaches the FCs of each cooperating node to all coded packets. The FCs are also calculated based on the channel quality:
\begin{equation}
 \label{eq:filteringprobability}
 fc(v,u)=\frac{1}{s}\cdot\left\lVert\cup_{u'\in X(v,u)}\mathbf{r}_{(v,u')}\right\rVert^2,
\end{equation}
where $X(v,u)$ is as previously in Equation~\ref{eq:prioirtymain} and $s$ is the size of the bit sequences (please note, that all of them have equal size).

\subsection{Compression Design}
\label{ssec:design}

In Table~\ref{tab:anchor} we present the overall structure of ANChOR headers and the corresponding sizes for each field it contains for an example node in a mesh network having 3 input and 3 output edges.

\begin{table}[htbp]
	\renewcommand{\arraystretch}{1.5}
	\centering
	\caption{ANChOR header structure and field sizes for an example node consisting of 3 input and 3 output edges.}
\begin{tabular}{|l|r|}
\hline 
\bf{Field description} & {\bf{Size}} (bytes) \\ 
\hline
\hline
\multicolumn{2}{|c|}{\emph{Part 1}} \\
\hline 
Coding vector ($\propto G_s$) & $32$ \\ 
\hline 
Generation ID & 1 \\
\hline 
CRC & 4 \\
\hline 
Symbol sequence number & 2 \\
\hline
\multicolumn{2}{|c|}{\emph{Part 2}} \\
\hline
\bf{Priority} & \bf{2} \\
\hline  
Sender ID & 1 \\
\hline
Flags & 1 \\
\hline
\multicolumn{2}{|c|}{\emph{Routing Protocol Information}} \\
\hline
\bf{Filtering probabilities} ($\propto |I(V)|$) & \bf{12} \\
\hline
Receiving map (maximum size, $\propto |O(V)|$) & 188 \\
\hline
Map of acknowledged generations ($\propto k$) & 4 \\
\hline
\multicolumn{2}{|c|}{\emph{Retransmission Request}} \\
\hline
Flags & 1 \\
\hline
Rank of coding matrices & 4 \\
\hline
Bitmap of uncoded symbols ($\propto k$) & 32 \\
\hline
Retransmission Request forwarder & 1 \\
\hline
%\hline
%\emph{Total size} & ? \\
%\hline
\end{tabular} 
\label{tab:anchor}
\end{table}

The priority value is calculated iteratively, which causes its instability (see Equation~\ref{eq:prioirtymain}). Also, it depends on the channel quality variation. ANChOR defines a term of significant priority difference $\Delta p$ to cope with this variation. Note, that the routing decision is taken based on the comparison of several priority values. The value $\Delta p$ is the width of a hysteresis for comparison of any two priority values. 

Following the same idea, we use a delta form of header compression. The delta compression, as the name suggest, relies on the transmission of a change in the field value between two consecutive transmissions.

Let's assume that $q_i$ is the uncompressed value of a given field in the $i^{th}$ packet and the directly preceding packet contains the $q_{i-1}$ value for the very same entry. In this case, the delta would be defined as follows:

\begin{equation}
\label{eq:delta}
\Delta(q_i, q_{i-1}) =
  \begin{cases}
    q_i - q_{i-1} ,& q_i > q_{i-1} \\
    q_{i-1} - q_i, & q_i < q_{i-1} \\
    0, & otherwise. \\
  \end{cases}
\end{equation}

With header compression only the difference between two values is transferred. Since this is smaller than $q_i$ and $q_{i-1}$, it requires less bits during transmission. By making sure that the size of the corresponding field adapts to the actual value of $\Delta(q_i, q_{i-1})$, we reduce the average header size.

Since one only transmits the difference between two values, the likelihood of the delta being close to zero is quite high, especially if the given field changes relatively ``slowly''. Consequently, the delta value could contain quite a number of leading zeroes, i.e., in the locations of the most significant bits. In which case these bits can be omitted during transmission. This is the basic principle of the \emph{Least Significant Bit} or \emph{LSB} compression. (We employ bit flags for the signalling of which interpretation of the delta value was used by the compressor.)

This technique requires the decompressor (at the receiver side) to have a reference value on which the delta is applied. We employ an advanced form of delta compression called \emph{variable length LSB compression} for the transmission of the field delta. This type of LSB compression, contrary to fixed--length LSB compression, can transmit up to 32 bits when necessary and is therefore usable for the initialisation of the decompressor reference value. It also uses the shortest amount of bytes possible for the delta transmission. For details we refer to~\cite{rfc5225}.

%variable length lsb compression::
%\begin{itemize}
%	\item lsb\_7 (1): 0x80 | value \& 0x7f
%	\item lsb\_14 (2): 0x40 | 0x3fff	
%	\item lsb\_21 (3): 0x20 | 0x1fffff
%	\item lsb\_29 (4): 0x10 | 0x0fffffff
%	\item full\_offset (5): 0x0fffffffff
%\end{itemize}

%if (this->priorityCurr - this->priorityPrev > this->sensitivity)

For the compression of the filtering probabilities we employ list compression as defined by~\cite{rfc5225}. Since the mechanics of list compression can be quite involved, we omit a detailed discussion of it. Very briefly, list compression was chosen because it can transmit a variable number of numeric items using LSB compression and therefore it is well suited for fields which can be interpreted as a list, such as the Contributing Source identifiers (CSRC) of a common RTP header. 

Since the filtering probabilities are sent for each connecting nodes including the node identifier and the probability value itself, list compression presents a suitable solution for the compression of this field. %We achieve this by transm

We also define a sensitivity property for the configuration which governs weather a small change in a field value is registered and consequently transmitted. This will in turn make the compression lossy in nature. This however should have only a small impact on the throughput of the whole network. The right choice of the sensitivity value is unfortunately not well defined. However, the discussion of this is outside the scope of this work and is part of the authors' ongoing research efforts.

\section{Channel Measurements}
\label{sec:mnat}

We conduct channel measurements to obtain traces of channel quality which we feed into the simulator. This allows the evaluation of realistic compression efficiency that can be expected in a real--life installation. We use WiFi routers as an example of OpR deployment in an IoT mesh network.

The compression efficiency depends on the variation of the header field values. The major variables that should be compressed are the \emph{priority} and the \emph{filtering coefficient}. These fields are calculated based on the estimations of the channel quality by each network node. The priority value $p(v)$ depends on the quality of the links between $v$ and its cooperating nodes $L(v)$, and also on the priorities of the cooperating nodes in general (see Equation~\ref{eq:prioirtymain}). Consequently, $p(v)$ iteratively depends on the quality of edges of the complete graph connecting the node $v$ with the destination. The filtering coefficients also depend on the complete graph connecting the node $v$ with the destination. However, a filtering coefficient is the discrete function of $p(u),\ \forall u\in L(v)$. Therefore, it is influenced more by the quality of links between the node $v$ and $u\in L(v)$.

The variation of both the priority and the filtering coefficients is affected by the variation of each edge quality on the graph connecting $v$ with the destination. Therefore, a realistic channel model plays a vital role in the estimation of the variation of the header field values.

We install a network containing 15 WiFi routers using IEEE802.11n/ac in a common office environment. The nodes are evenly distributed to achieve
homogeneous coverage. The longest distance between the installed nodes is 46 meters and the shortest is 5 meters. No two nodes (except the furthest ones located along a corridor) have a direct line of sight. For signal amplification, we used 5dB antennas. The \textit{ath9k} WiFi card driver allows us to setup different Modulation and
Coding Schemes (MCS) for each packet sent and to turn off MAC retransmissions. Hence, each packet is transmitted only once. IEEE Std 802.11 implements the Frame Check Sequence (FCS) field which is attached to each packet. This was used for the verification of the successful decoding of a packet decoding. For each received packet, we store one bit that corresponds to either the failure or the success of the decoding process. 

The channel measurement is conducted as follows. Each node randomly chooses the time period before sending a set of 64 packets consisting of 1400 bytes. No coordination between the nodes is implemented. The expectation value of the time period is selected to be large enough to avoid simultaneous transmissions of at least two nodes with 99\% probability. This way we avoid capturing the effect of interference created by several nodes transmitting at the same time. Therefore, the probability of packet reception is purely a function of the distance between nodes and the obstacles located between them.

After the transmission of each set of 64 packets the node selects a different MCS. This allowed the selection of any sending rate corresponding to the MCSs available in IEEE802.11n/ac \cite{mcsindex}. The OpR selects the MCS for each sending node while trying to maximise the effective data rate between a given node and the destination. In \cite{diss}, the algorithm for the rate selection is described, which is also implemented in the simulator.

% In this way, we collect the the bit sequences of FCS failures or successes for each MCS for each edge. The simulator uses the FCS status at the receivers to decide if the packet is successfully decoded. Each time a packet is sent, the simulator rotates the bit sequences on one bit for all edges emulating the flow of time.

Based on the measured channel quality, we can evaluate the level of logical interconnection in our testbed. The higher the interconnection the more variables influence the priority and filtering coefficients and a higher variation on the header field sizes is expected. In Figure~\ref{fig:cdf_coopgroupsize}, we show the Cumulative Density Function (CDF) of the receiver group size. Here, we included all nodes that can decode the data from the given sender when it uses at least the most robust MCS.

\begin{figure}[!t]
\centering
    \includegraphics[width=0.90\columnwidth]{imgs/MatePaper.pdf}
\caption[caption]{CDF of the receiver group size}
\label{fig:cdf_coopgroupsize}
\end{figure}

From the variety of one--source--destination scenarios, we selected three topologies shown in Figure~\ref{fig:scenarioA}--\ref{fig:scenarioC}. Although we use bit sequences of FCS status for packet erasure emulation, in these figures we show the overall average values of the loss ratios to describe the channel quality.

\begin{figure}[!t]
 \centering
    \begin{subfigure}[b]{0.23\textwidth}
      \scenarioA
      \caption{$d(v_0)=58.5$ Mbps, \\$d(v_1)=d(v_4)=26$ Mbps, \\$d(v_2)=d(v_3)=104$ Mbps}
      \label{fig:scenarioA}
    \end{subfigure}
    \begin{subfigure}[b]{0.25\textwidth}
      \scenarioB
      \caption{$d(v_0)=d(v_2)=104$ Mbps, \\$d(v_1)=d(v_3)=26$ Mbps}
      \label{fig:scenarioB}
    \end{subfigure}    
    \begin{subfigure}[b]{0.48\textwidth}
      \centering
      \vspace{0.2in}
      \scenarioC
      \caption{$d(v_0)=104$ Mbps, $d(v_1)=d(v_2)=26$ Mbps}
      \label{fig:scenarioC}
    \end{subfigure}   
    \caption[caption]{Simulation scenarios; source: $v_0$; destination: $v_4$ in (a), $v_3$ in (b), $v_2$ in (c); $d(v)$ is a sending data rate of $v$}
\end{figure}

\section{Simulation and Results}
\label{sec:simres}

%\subsection{Simulation Environment}

We use the discrete--event simulator from \cite{diss} that implements a broadcast packet erasure channel and allows the setting up of arbitrary network topologies. It also implements a greedy traffic generator. It is most suitable for the analysis of the maximum achievable data rate that can be sustained by a given network. The channel access is realised using the principle of slotted ALOHA.  At the beginning of each slot, all vertices that have data available for transmission get channel access with a probability $1/n$, where $n$ is the number of vertices. In contrast to the traditional slotted ALOHA principle, in our implementation the collisions are avoided with a central coordinator function. 

%%\subsection{Metrics}
%%\label{s:methods}

%\ref{} def for throughput \ref{}

%%Let $UH_i$ denote the size of the uncompressed protocol headers in the $i^{th}$ packet and $CH_i$ denote its compressed header size. In turn, we derive performance measures as the actual savings (or alternatively the gain) of the encapsulation overhead (headers) as
%%\begin{equation}
%%\label{eq:actSavH}
%%SH_i = \frac{UH_i - CH_i}{{UH_i}}.
%%\end{equation}

%%In order to accurately quantify the usefulness of the compression, it is important to take into account parameters that potentially decrease the overall compression gain. In this paper we define a utility function as
%%\begin{equation}
%%  \begin{aligned}
%%    u_i(\mathbf{x}) = \lbrace \mathbf{x} \in \mathbf{R}^k \text{ : } & u_i(\mathbf{x}) \in [-1.0,1.0] \subset \mathbf{R} \\ 
%%    &\textit{ and } k, i \in \mathbf{Z} \rbrace, \nonumber
%%  \end{aligned}
%%\end{equation}
%%where $\mathbf{x}$ is a vector of the $k$ observed variables' value for a given packet $i$. A $u_i$ value of $1.0$ is interpreted as the optimal utility and a $-1.0$ value as most disadvantageous. 

% check if this is the case for the custom compression

%Typical conditions that counterbalance optimal utility that are usually considered are decompression failure and decompressor feedback. 

%If there are decompression failures (commonly due to decompressor desynchronisation caused by extensive packet losses on the channel), each compressed packet failing decompression was sent completely in vain and it is usually forbidden to interpret such packets. In this case, each byte of the compressed packet decreases the compression gain as well, since the transmission of such packets wastes bandwidth.

% No utility is discussed

%%During compression, feedback is produced by the decompressor and sent back to the compressor entity. Since compression feedback is generated by the compression layer and not as part of the original stream, each feedback byte decreases the overall gain. Note, that we ommit that handling of decompressor feedback as, in our opinion, the reverse propagation of compression reletad information is not necessary for a setup like this and the applied  compression techniques function quite well without this feature. Normally, compression techniques which ommit certain fields during transmission and solely rely on inference from previous transmissions warrant a need for decompressor feedback. See \ref{} RFC \ref{} for details.

%%Consequently, we consider the following approach for the generation of the utility function.  Let $SH_i$ denote the $i^{th}$ packet's savings as in Equation~\ref{eq:actSavH}. In turn, we determine the utility of the $i^{th}$ packet as 
%%\begin{equation}
%%\label{eq:utilityFull}
%%U_i = \sum_{i=0}^N(\phi (i) \cdot SH_i),
%%\end{equation}
%%where the indicator function $\phi (i)$ represents the decompression success of the $i^{th}$ packet as 
%%\begin{equation}
%%\label{eq:phi}
%%\phi (i) =
%%  \begin{cases}
%%    -1, & \textit{decompression of the } i^{th} \textit{ packet failed} \\
%%    \quad 1, & \textit{otherwise.}\\
%%  \end{cases}
%%\end{equation}

%\ref{} bla about Figure~\ref{fig:overhead} \ref{} 

% \begin{figure}[h]	
% 	\centering
% 	\includegraphics[width=1.0\linewidth]{./imgs/diamond/resource_waste.pdf}
% 	\caption{Placeholder for protocol overhead figure.\label{fig:overhead}}	
% \end{figure}

%\section{Results}
%\label{sec:res}

In the following subsections we summarise the current state of results based on the simulation results obtain for the discussed topologies and the compression applied to the priority and filtering probability fields.

\subsection{Simulation}

The channel quality captured during the testbed measurement is highly dependent on time. In Figure~\ref{fig:losses}, one can see the loss ratios for the outgoing edges of the node $v_0$ for the topology in Figure~\ref{fig:scenarioA}.

\begin{figure}[!t]
\centering
    \includegraphics[width=0.90\columnwidth]{imgs/topology3/real/loss_ratios_0.pdf}
\caption[caption]{Loss ratios for topology in Figure~\ref{fig:scenarioA} with non--stationary channel setup (outgoin edges of node $v_0$)}
\label{fig:losses}
\end{figure}

For the same topology, we show the priority values at each node in Figure~\ref{fig:priornonst}. We explore the reason for the priority variation showing the simulation results for an analog scenario with the stationary channel. We setup the network with the same topology (Figure~\ref{fig:scenarioA}) and loss probabilities equal to the overall average probabilities for the whole duration of the measurement on the testbed (basically, the values shown in Figure~\ref{fig:scenarioA}). The corresponding priority values are shown in Figure~\ref{fig:priorst}. They are much more stable than those in Figure~\ref{fig:priornonst}. This means that the iterative nature of the priority calculation has a minor contribution on the variation of priority in a real network. Hence, the usage of the measured traces is crucial for the understanding of the possible gain from header compression.


\begin{figure}[!t]
 \centering
    \begin{subfigure}[b]{0.90\columnwidth}
      \includegraphics[width=1\columnwidth]{imgs/topology3/real/priorities_4.pdf}
      \caption{Non-stationary channel setup}
      \label{fig:priornonst}
    \end{subfigure}
    \begin{subfigure}[b]{0.90\columnwidth}
      \includegraphics[width=1\columnwidth]{imgs/topology3/stationary/priorities_4.pdf}
      \caption{Stationary channel setup}
      \label{fig:priorst}
    \end{subfigure}      
    \caption[caption]{Priority for topology in Figure~\ref{fig:scenarioA}}
\end{figure}

In Figure~\ref{fig:datarate}, we show the data rate for all three topologies as a function of the window size that each node applies on the sequences of the FCS status to calculate the loss ratios. The bigger the window, the more stable are the obtained values. However, the routing protocol will react to the changes on the channel quality relatively slowly and becomes suboptimal. Surprisingly, despite the high variation of the loss ratios over time, the effective data rate measured at the destination does not change significantly (the confidence intervals are 95\%). 

\begin{figure}[!t]
\centering
    \includegraphics[width=0.90\columnwidth]{imgs/datarate.pdf}
\caption[caption]{Data rate}
\label{fig:datarate}
\end{figure}

\subsection{Compression}

First we evaluate the compression for the priority and filtering probability fields in terms of the average achieved compression gain and the mean error introduced by the sensitivity property discussed in Section~\ref{ssec:design}.

In Figure~\ref{fig:errtop3} we observe the mean error for topology 1 (Figure~\ref{fig:scenarioA}) at each node in the mesh. We see that even though lossyness was introduced into the compression, the normalised error is quite small and is between $0.01$ and $0.02$. However, the filtering probabilities only add error on the $v_3$ node and is still under $0.05$ on average. The reason behind this is that most of the filtering probabilities of the nodes either stay constant or converge fast toward a stable value, which makes the difference in field value higher than the used sensitivity.

When we look at the compression gain for both of the header fields we see that the priority is compressed at least 40~\% (which is 0.6 times the original size of the field). The gain for the filtering probabilities, however, can be much higher and is between 50~\% and 85~\% on average. Since the list compression omits the transmission of previously sent values, with the right sensitivity setting the compressed list can be very small or even empty. Considering that without compression each of these probabilities are sent, this proved to be a very good choice to tackle compression.

\begin{figure}
 \centering
    \begin{subfigure}[b]{0.85\columnwidth}
      \includegraphics[width=1\columnwidth]{imgs/comp/errorTop3_1}
      \caption{Mean compression error}
      \label{fig:errtop3}
    \end{subfigure}
    \begin{subfigure}[b]{0.85\columnwidth}
      \includegraphics[width=1\columnwidth]{imgs/comp/gainTop3_1}
      \caption{Mean compression gain}
      \label{fig:gaintop3}
    \end{subfigure}      
    \caption[caption]{Compression of priority and filtering probabilities node--by--node for topology 1 (Figure~\ref{fig:scenarioA}) with 95\% confidence intervals.}
\end{figure}

When we look at all three topologies on Figures~\ref{fig:pf} and~\ref{fig:gain} we see a similar picture. The priority error is under $0.04$. The cause for topology 1 having an average error value of $0$ is due to the values changing rapidly or being constant during the transmissions. Also, the average compression gain is at least 62~\% for all examined topologies.

\begin{figure}
 \centering
    \begin{subfigure}[b]{0.90\columnwidth}
      \includegraphics[width=1\columnwidth]{imgs/comp/pf}
      \caption{Mean compression error}
      \label{fig:pf}
    \end{subfigure}
    \begin{subfigure}[b]{0.90\columnwidth}
      \includegraphics[width=1\columnwidth]{imgs/comp/gain}
      \caption{Mean compression gain}
      \label{fig:gain}
    \end{subfigure}      
    \caption[caption]{Compression of priority and filtering probabilities for each discussed topology  and averaging window for non--stationary topologies and a stationary topology (denoted as \emph{static}) with a window size of 1000. All of them with 95\% confidence intervals.}
\end{figure}

\section{Conclusions}
\label{sec:conclusions}

In the future, IoT devices and applications have to deal with a high density and a limited available bandwidth. One solution to this problem is provided by the seamless interconnection of devices in a wireless mesh network. In this case, the communication between source and destination could utilise multiple channels at the same time with the proper routing protocol playing a crucial part in most of the key performance measures.

The routing performance can be significantly increased with the employment of opportunistic decisions. However, this method relies heavily on feedback, which exponentially increases with the size of the network and could pose a major setback.

Various header compression schemes are already being used by IP--capable networks to limit transmission overhead, and the application of such methods are a natural solution to the large routing messages. We saw that the employment of just a few compression steps reduces the size of these transmissions significantly, which would ensure a higher overall throughput and a lower energy footprint due to diminished network interface activity. Both of these playing a big role in IoT deployment.

Our simulations based on real world traces captured on a mesh network show that the node priorities can be quite stable in comparison to the frequency of sending the packets. As a result, the corresponding field of the packet header becomes a good candidate for LSB compression. In general, we were able to achieve at least 40~\% gain on such fields alone. The filtering probabilities also play a crucial role in the routing protocol, because the OpR header includes the filtering values for each cooperating node. The list compression applied on these values proved to be able to compress about 50--85~\% of their original sizes.

In the future, a we plan to obtain a more thorough image of how compression impacts the routing protocol, as higher loss ratios on a given channel could corrupt the decompressor contexts. This would in turn potentially affect the overall throughput of the given mesh network. An in--depth investigation of the sensitivity configuration would also prove quite beneficial, since finding the right trade--off between lossyness of compression (i.e., gain) and routing accuracy is not necessarily obvious.

\section*{Acknowledgments}
This work was supported by the National Research, Development and Innovation Fund of Hungary in the frame of \mbox{FIEK 16--1--2016--0007} (Higher Education and Industrial Cooperation Center) project and by the \mbox{\'UNKP--17--4--IV} New National Excellence Program of the Ministry of Human Capacities and by the German Federal Ministry for Education and Research (TacNet, FKZ: \mbox{16KIS0736})

\bibliographystyle{ieeetr}
\balance
\bibliography{main}


\end{document}
